Spaceship Engine is a complete project system that will help you in creating your game.

 
Supported Platforms   
    PC
    Android, IOS, XBOX (the system works on these platforms, but control is not done )

 
Asset contains   
    1. Controlling the ship
        * advanced ship control system
        * PBR cabin
        * hands are animated
        * rotation, height control
        * switching weapons
        * head rotation
        * target capture
        * target acquisition compass
        * horizon compass
        * shader hitting the projectile in the energy shield

    2. Opponents
        * AI
        * enemy ships (5 ships)
        * turrets (tracks the target regardless of its own position in space)
        * station

    3. Systems
        * asteroid field (interactive)
        * spawn enemies (field for spawning and movement of opponents)
        * object pooler
        * character properties controller 

    4. Other
        * sounds
        * particles
        * base shader (optimized standard Unity shader) 
   
 
Control
    left mouse button   - shoot
    right mouse button  - change weapons
    mouse wheel button  - look around
    Shift               - acceleration
    R / Q               - spin
    Ctrl / Space        - height
    Tab                 - target lock 


Sounds were taken from https://freesound.org 
Skybox were taken from Skybox Series Free by Avionx


Feedback(suggestions, questions, reports or errors)
    SomeOneWhoCaresFeedBack @gmail.com


My social networks
    https://www.artstation.com/vrdsgsegs
    https://twitter.com/iOneWhoCares