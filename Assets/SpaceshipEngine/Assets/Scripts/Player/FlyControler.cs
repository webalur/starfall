using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyControler : MonoBehaviour
{

    public CharacterController controller;
    public Transform cameraHolder;
    public Rigidbody rb;
    public Transform head;
    public Transform rotate;
    public float speed = 500f;
    public int shiftSpeed = 200;
    public float spinAroundAxis = 8;

    public GameObject furiousWarp;
    public GameObject furiousWarpChild;
    public GameObject playerShip;

    public bool inWarpByKey = false;
    public bool inWarpByUIButton = false;

    Vector3 move;

    SpawnEnemies spawnEnemies;
    GlassController glassController;

    public Animator _animatorLeftHand;
    public Animator _animatorRightHand;

    public AudioSource engineSound;


    private void Start()
    {
        spawnEnemies = FindObjectOfType<SpawnEnemies>();
        glassController = FindObjectOfType<GlassController>();
    }

    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        float y = 0;

        if (Input.GetKey(KeyCode.LeftShift))
            engineSound.pitch = Mathf.Lerp(engineSound.pitch, Mathf.Abs(z) + 1.5f, Time.deltaTime * 2);
        else
            engineSound.pitch = Mathf.Lerp(engineSound.pitch, Mathf.Abs(z) + 0.5f, Time.deltaTime * 2);


        _animatorRightHand.SetFloat("MoveSide", x);
        _animatorRightHand.SetFloat("MoveForward", z);

        if (Input.GetKey(KeyCode.Space))
            y = 1;
        if (Input.GetKey(KeyCode.LeftControl))
            y = -1;

        if (Input.GetKey(KeyCode.LeftShift) && inWarpByUIButton==false)
        {
            inWarpByKey = true;

            move = transform.right * x + transform.forward * z * shiftSpeed + transform.up * y;
            cameraHolder.localPosition = Vector3.Lerp(cameraHolder.localPosition, new Vector3(0,0,-0.2f), Time.deltaTime * 3);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 70, Time.deltaTime * 3);

            _animatorLeftHand.SetInteger("Shift", 1);
            // furiousWarp.SetActive(true);

            // furiousWarp.transform.rotation = transform.rotation;
            // furiousWarp.transform.position = playerShip.transform.position;
            // furiousWarpChild.transform.rotation = transform.rotation;
            // furiousWarpChild.transform.position = playerShip.transform.position;

            
        }
        else
        {
            move = transform.right * x + transform.forward * z + transform.up * y;
            _animatorLeftHand.SetInteger("Shift", 0);

            furiousWarp.SetActive(false);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            inWarpByKey = false;
        }
            
            rb.AddForce(move, ForceMode.Impulse);
        
            

        //move head when moves to the side
        if (x > 0)
        {
            head.Rotate(-Vector3.up * 0.25f);
            head.Rotate(-Vector3.forward * 0.4f);
        }
        if (x < 0)
        {
            head.Rotate(Vector3.up * 0.25f);
            head.Rotate(Vector3.forward * 0.4f);
        }

        //spin around an axis
        if (Input.GetKey(KeyCode.Q))
        {
            rotate.Rotate(0, 0, 1);
            head.Rotate(Vector3.forward);
        }
        if (Input.GetKey(KeyCode.E))
        {
            rotate.Rotate(0, 0, -1);
            head.Rotate(-Vector3.forward);
        }

        //camera controll when attack
        if (Input.GetMouseButton(0))
        {
            cameraHolder.localPosition = Vector3.Lerp(cameraHolder.localPosition, new Vector3(0, 0, 0.07f), Time.deltaTime * 3);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 55, Time.deltaTime * 3);
        }

        //return camera to start position
        if (Input.GetMouseButton(0) == false && Input.GetKey(KeyCode.LeftShift) == false)
        {
            if (cameraHolder.localPosition.z != 0.0f)
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 60, Time.deltaTime * 3);
                cameraHolder.localPosition = Vector3.Lerp(cameraHolder.localPosition, Vector3.zero, Time.deltaTime * 3);
            }
        }

        //choose target
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            float closestToCamera = Mathf.Infinity;
            int index = -1;

            for (int i = 0; i < spawnEnemies.allEnemies.Count; i++)
            {
                if (spawnEnemies.allEnemies[i] != null) 
                {
                    Vector3 targetDir = spawnEnemies.allEnemies[i].transform.position - transform.position;
                    float angleBetween = Vector3.Angle(transform.forward, targetDir);

                    if (angleBetween < closestToCamera && angleBetween < 25)
                    {
                        closestToCamera = angleBetween;
                        index = i;
                    }
                }
            }

            if (index != -1)
            {
                glassController.target = spawnEnemies.allEnemies[index].transform;
                glassController.aim.GetComponent<ParticleSystem>().Play(true);
            }
        }
    }

}
