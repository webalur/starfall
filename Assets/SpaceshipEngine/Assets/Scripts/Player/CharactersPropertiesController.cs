using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharactersPropertiesController : MonoBehaviour
{
    //player
    public int playerBullerDamage = 100;
    public int playerRocketDamage = 200;
    public int playerShieldEnergy = 100;
    public int playerHP = 1000;

    //turret
    public int turretBulletDamage = 20;
    public int turretHP = 200;

    //enemy ship
    public int enemyBulletDamage = 15;
    public int enemyHP = 150;

    public void GetDamage(int damage)
    {
        playerHP -= damage;
    }
}


