﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunsController : MonoBehaviour
{
    public Transform ship;

    public Transform leftGun;
    public Transform rightGun;

    public ParticleSystem leftGunParticle;
    public ParticleSystem rightGunParticle;

    public Transform leftRocket;
    public Transform rightRocket;

    public ParticleSystem leftRocketParticle;
    public ParticleSystem rightRocketParticle;

    Animator animator;

    float timerLeftGun;

    CameraShake cameraShake;
    ObjectPooler objectPooler;

    public AudioSource reloadAudio;

    public AudioSource leftGunAudio;
    public AudioSource rightGunAudio;
    public AudioSource leftRocketAudio;
    public AudioSource rightRocketAudio;

    private void Start()
    {
        animator = this.GetComponent<Animator>();
        cameraShake = FindObjectOfType<CameraShake>();

        objectPooler = ObjectPooler.SharedInstance;
        Random.InitState((int)System.DateTime.Now.Ticks);
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            animator.SetBool("attack", true);
            animator.SetBool("attackRocket", true);
        }
        else
        {
            animator.SetBool("attack", false);
            animator.SetBool("attackRocket", false);
        }

        if (Input.GetMouseButtonDown(1))
        {
            animator.SetBool("reload", !animator.GetBool("reload"));
            reloadAudio.Play();
        }
    }

    public void LeftGunShoot()
    {
        leftGunParticle.Play(true);
        LaunchBullet(leftGun, 0, 300, leftGunAudio);
    }

    public void RightGunShoot()
    {
        rightGunParticle.Play(true);
        LaunchBullet(rightGun, 0, 300, rightGunAudio);
    }

    public void LeftRocketShoot()
    {
        leftRocketParticle.Play(true);
        LaunchBullet(leftRocket, 3, 200, leftRocketAudio);
    }

    public void RightRocketShoot()
    {
        rightRocketParticle.Play(true);
        LaunchBullet(rightRocket, 3, 200, rightRocketAudio);
    }

    public void LaunchBullet(Transform obj, int bulletIndex, int bulletSpeed, AudioSource sound)
    {
        cameraShake.shakeDuration = 0.01f;
        GameObject ps = objectPooler.GetPooledObject(bulletIndex);
        Rigidbody rb = ps.GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        ps.transform.position = obj.position;
        ps.transform.forward = obj.forward;
        ps.gameObject.SetActive(true);
        rb.AddForce(ps.transform.forward * bulletSpeed, ForceMode.Impulse);

        sound.Play();
    }

}
