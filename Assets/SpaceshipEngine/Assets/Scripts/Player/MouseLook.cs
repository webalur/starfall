﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public float mouseSensitivity = 100f;
    public Transform playerBody;
    public Transform ship;
    public Transform head;
    public Transform cameraHolder;
    public float shipToCameraSpeed = 3;

    float xRotation, yRotation = 0;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        if (mouseX > 10) mouseX = 10;
        if (mouseY > 10) mouseY = 10;
        if (mouseX < -10) mouseX = -10;
        if (mouseY < -3) mouseY = -4;

        //head or ship rotation
        if (Input.GetMouseButton(2))
        {
            //head rotation
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, - 70f, 30f);

            //head.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            cameraHolder.Rotate(Vector3.up * mouseX);
            head.Rotate(Vector3.right * -mouseY);

            //clamp head rotation
            cameraHolder.localRotation = new Quaternion(0, Mathf.Clamp(cameraHolder.localRotation.y, -0.55f, 0.55f), 0, cameraHolder.localRotation.w);
            if (Input.GetKey(KeyCode.LeftShift) == false)
                head.localRotation = new Quaternion(Mathf.Clamp(head.localRotation.x, -0.55f, 0.24f), 0, 0, head.localRotation.w);
            else
                head.localRotation = new Quaternion(Mathf.Clamp(head.localRotation.x, -0.55f, 0.12f), 0, 0, head.localRotation.w);
            //head.localRotation = Quaternion.Euler(head.localEulerAngles.x, head.localEulerAngles.y, 0);
        }
        else
        {
            //ship rotation
            playerBody.Rotate(Vector3.up * mouseX);
            playerBody.Rotate(Vector3.right * -mouseY);

            if (head.localRotation != Quaternion.identity)
                head.localRotation = Quaternion.Lerp(head.localRotation, Quaternion.identity, Time.deltaTime * 3f);

            if(cameraHolder.localRotation != Quaternion.identity)
                cameraHolder.localRotation = Quaternion.Lerp(cameraHolder.localRotation, Quaternion.identity, Time.deltaTime * 3f);
        }

        ship.transform.rotation = Quaternion.Lerp(ship.transform.rotation, playerBody.transform.rotation, Time.deltaTime * shipToCameraSpeed);
    }

}
