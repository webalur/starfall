﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassController : MonoBehaviour
{

    public Transform target;

    public Transform aim;
    public Transform findTarget;
    [HideInInspector]
    public Transform glass;

    float damage1rad;
    float damage2rad;
    float damage3rad;

    public Transform[] damagePositions;

    public AudioSource expliosion;
    public AudioSource shield;

    Vector3 lookPos;
    Quaternion lookRot;
    Quaternion rotation;
    Vector3 lookDir;

    CharactersPropertiesController charactersPropertiesController;

    int plaherShield;

    private void Awake()
    {
        charactersPropertiesController = FindObjectOfType<CharactersPropertiesController>();

        glass = this.transform;
        damagePositions = new Transform[3];

        Shader.SetGlobalFloat("_Damage1Rad", 0);
        Shader.SetGlobalFloat("_Damage2Rad", 0);
        Shader.SetGlobalFloat("_Damage3Rad", 0);

        plaherShield = charactersPropertiesController.playerShieldEnergy;
    }

    void Update()
    {
        //find target
        if (target != null)
        {
            findTarget.gameObject.SetActive(true);
            findTarget.LookAt(target.position);
        }
        else
        {
            findTarget.gameObject.SetActive(false);
        }

        //aim
        if (target != null)
        {
            aim.gameObject.SetActive(true);
            aim.LookAt(Camera.main.transform);
            aim.position = target.position;

            float scale = Vector3.Distance(this.transform.position, target.position) / 4;
            if (scale < 20)
                scale = 20;
            aim.localScale = new Vector3(scale, scale, scale);
        }
        else
        {
            aim.gameObject.SetActive(false);
        }

        //shader glass damage
        if (damagePositions[0] != null)
        {
            Shader.SetGlobalVector("_Damage1Pos", damagePositions[0].position);

            damage1rad += Time.deltaTime * 2;
            Shader.SetGlobalFloat("_Damage1Rad", damage1rad);
            if (damage1rad > 1)
            {
                damagePositions[0].gameObject.SetActive(false);
                damagePositions[0] = null;
                damage1rad = 0;
                Shader.SetGlobalFloat("_Damage1Rad", damage1rad);
            }
        }

        if (damagePositions[1] != null)
        {
            Shader.SetGlobalVector("_Damage2Pos", damagePositions[1].position);

            damage2rad += Time.deltaTime * 2;
            Shader.SetGlobalFloat("_Damage2Rad", damage2rad);
            if (damage2rad > 1)
            {
                damagePositions[1].gameObject.SetActive(false);
                damagePositions[1] = null;
                damage2rad = 0;
                Shader.SetGlobalFloat("_Damage2Rad", damage2rad);
            }
        }

        if (damagePositions[2] != null)
        {
            Shader.SetGlobalVector("_Damage3Pos", damagePositions[2].position);

            damage3rad += Time.deltaTime * 2;
            Shader.SetGlobalFloat("_Damage3Rad", damage3rad);
            if (damage3rad > 1)
            {
                damagePositions[2].gameObject.SetActive(false);
                damagePositions[2] = null;
                damage3rad = 0;
                Shader.SetGlobalFloat("_Damage3Rad", damage3rad);
            }
        }
    }

    public void TakeDamage(int damage)
    {
        plaherShield -= damage;
        shield.Play();
        expliosion.Play();
    }

}
