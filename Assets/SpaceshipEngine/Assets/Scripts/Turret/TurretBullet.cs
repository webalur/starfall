﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{

    GlassController glassController;
    ObjectPooler objectPooler;
    GameObject ps;


    private void Start()
    {
        glassController = FindObjectOfType<GlassController>();

        objectPooler = ObjectPooler.SharedInstance;
        ps = objectPooler.GetPooledObject(2);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //explosion
        GameObject obj = objectPooler.GetPooledObject(4);
        obj.SetActive(true);
        obj.transform.position = this.transform.position;
        obj.transform.parent = objectPooler.transform;


        //damage player glass
        if (collision.collider.tag == "PlayerShip")
        {
            for (int i = 0; i < glassController.damagePositions.Length; i++)
            {
                if(glassController.damagePositions[i] == null)
                {
                    ps.transform.position = collision.contacts[0].point;
                    ps.transform.parent = glassController.glass;
                    glassController.damagePositions[i] = ps.transform;
                    break;
                }
            }
        }

        obj.transform.parent = glassController.glass;

        this.gameObject.SetActive(false);
    }


}
