﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{

    Transform player;
    public Transform turretBase;
    public Transform turretGun;
    public Transform bulletPos;

    int HP = 500;
    int currentHP;

    public int bulletSpeed = 50;

    public int range = 100;

    public ParticleSystem shootPS;

    Animator _animator;
    ObjectPooler objectPooler;

    Vector3 lookPos;
    Quaternion lookRot;
    Quaternion rotation;
    Vector3 lookDir;

    int cubeLayerIndex;

    CharactersPropertiesController charactersPropertiesController;
    SpawnEnemies spawnEnemies;

    bool rotateToStartPosition;

    void Start()
    {
        spawnEnemies = FindObjectOfType<SpawnEnemies>();

        objectPooler = ObjectPooler.SharedInstance;

        player = FindObjectOfType<FlyControler>().transform;
        _animator = this.GetComponent<Animator>();

        cubeLayerIndex = LayerMask.NameToLayer("PlayerGlass");

        charactersPropertiesController = FindObjectOfType<CharactersPropertiesController>();
        HP = charactersPropertiesController.turretHP;
        currentHP = HP;
    }

    public void GetDamage(int damage)
    {
        currentHP -= damage;
    }

    void Update()
    {
        rotateToStartPosition = true;

        if (currentHP <= 0)
        {
            GameObject ps = objectPooler.GetPooledObject(5);
            ps.transform.position = this.transform.position;
            ps.SetActive(true);
            Destroy(this.transform.parent.gameObject);
        }

        if (Vector3.Distance(player.position, this.transform.position) < range)
        {
            AttackEnemy(player);
            rotateToStartPosition = false;
        }
        else
        {
            for (int i = 0; i < spawnEnemies.allSpaceShips.Count; i++)
            {
                if (spawnEnemies.allSpaceShips[i] != null) {
                    if (Vector3.Distance(spawnEnemies.allSpaceShips[i].transform.position, this.transform.position) < range)
                    {
                        AttackEnemy(spawnEnemies.allSpaceShips[i].transform);
                        rotateToStartPosition = false;
                        break;
                    }
                }
            }
        }

        if (rotateToStartPosition)
        {
            turretBase.localRotation = Quaternion.Lerp(turretBase.localRotation, Quaternion.identity, Time.deltaTime * 0.8f);
            turretGun.localRotation = Quaternion.Lerp(turretGun.localRotation, Quaternion.identity, Time.deltaTime * 0.8f);
            _animator.SetBool("attack", false);
        }
    }

    public void SendBullet()
    {
        GameObject ps = objectPooler.GetPooledObject(1);
        Rigidbody rb = ps.GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        ps.transform.position = bulletPos.position;
        ps.transform.forward = bulletPos.forward;
        ps.gameObject.SetActive(true);
        rb.AddForce(ps.transform.forward * bulletSpeed, ForceMode.Impulse);

        shootPS.Play(true);
    }

    void AttackEnemy(Transform enemy)
    {
        //base rotation
        lookRot = turretBase.rotation;
        turretBase.LookAt(enemy.position);
        rotation = Quaternion.Euler(0, turretBase.localRotation.eulerAngles.y, 0);
        turretBase.rotation = lookRot;
        turretBase.localRotation = Quaternion.Lerp(turretBase.localRotation, rotation, Time.deltaTime * 2f);

        //gun rotation
        lookRot = turretGun.rotation;
        turretGun.LookAt(enemy.position);

        float angle = turretGun.localEulerAngles.x;
        angle = (angle > 180) ? angle - 360 : angle;
        if (angle > 15) angle = 15;
        if (angle < -85) angle = -85;
        rotation = Quaternion.Euler(angle, 0, 0);

        turretGun.rotation = lookRot;
        turretGun.localRotation = Quaternion.Lerp(turretGun.localRotation, rotation, Time.deltaTime * 2f);

        Debug.DrawRay(bulletPos.position, bulletPos.forward * range, Color.red);

        //attack
        //int layerMask = (1 << cubeLayerIndex);
       // RaycastHit hit;
       // if (Physics.Raycast(bulletPos.position, bulletPos.TransformDirection(Vector3.forward), out hit, range, layerMask))
       // {
            _animator.SetBool("attack", true);
       // }
       // else
       // {
        //    _animator.SetBool("attack", false);
       // }
    }

}
