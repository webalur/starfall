﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SolidRectangleExample))]
public class DrawSolidRectangle : Editor
{

    void OnSceneGUI()
    {
        SolidRectangleExample t = target as SolidRectangleExample;
        Vector3 pos = t.transform.position;

        Vector3[] verts = new Vector3[]
        {
            new Vector3(pos.x - t.range, pos.y - t.range, pos.z - t.range),
            new Vector3(pos.x - t.range, pos.y - t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y - t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y - t.range, pos.z - t.range),
        };

        Vector3[] verts2 = new Vector3[]
        {
            new Vector3(pos.x - t.range, pos.y + t.range, pos.z - t.range),
            new Vector3(pos.x - t.range, pos.y + t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y + t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y + t.range, pos.z - t.range),
        };

        Vector3[] verts3 = new Vector3[]
        {
            new Vector3(pos.x - t.range, pos.y - t.range, pos.z - t.range),
            new Vector3(pos.x - t.range, pos.y + t.range, pos.z - t.range),
            new Vector3(pos.x + t.range, pos.y + t.range, pos.z - t.range),
            new Vector3(pos.x + t.range, pos.y - t.range, pos.z - t.range),
        };

        Vector3[] verts4 = new Vector3[]
        {
            new Vector3(pos.x - t.range, pos.y - t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y - t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y + t.range, pos.z + t.range),
            new Vector3(pos.x - t.range, pos.y + t.range, pos.z + t.range),
        };

        Vector3[] verts5 = new Vector3[]
        {
            new Vector3(pos.x + t.range, pos.y - t.range, pos.z - t.range),
            new Vector3(pos.x + t.range, pos.y - t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y + t.range, pos.z + t.range),
            new Vector3(pos.x + t.range, pos.y + t.range, pos.z - t.range),
        };

        Vector3[] verts6 = new Vector3[]
        {
            new Vector3(pos.x - t.range, pos.y - t.range, pos.z + t.range),
            new Vector3(pos.x - t.range, pos.y - t.range, pos.z - t.range),
            new Vector3(pos.x - t.range, pos.y + t.range, pos.z - t.range),
            new Vector3(pos.x - t.range, pos.y + t.range, pos.z + t.range),
        };


        Handles.DrawSolidRectangleWithOutline(verts, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));
        Handles.DrawSolidRectangleWithOutline(verts2, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));
        Handles.DrawSolidRectangleWithOutline(verts3, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));
        Handles.DrawSolidRectangleWithOutline(verts4, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));
        Handles.DrawSolidRectangleWithOutline(verts5, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));
        Handles.DrawSolidRectangleWithOutline(verts6, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));

       /* foreach (Vector3 posCube in verts)
        {
            t.range = Handles.ScaleValueHandle(t.range,
                posCube,
                Quaternion.identity,
                1.0f,
                Handles.CubeHandleCap,
                1.0f);
        }*/
    }
}
