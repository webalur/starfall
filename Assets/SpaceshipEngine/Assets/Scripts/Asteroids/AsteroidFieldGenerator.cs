﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidFieldGenerator : MonoBehaviour
{

    public Transform[] asteroids;
    public int fieldRadius = 100;
    public int asteroidAmount = 100;

    public float maxScale = 10f;
    public float minScale = 1f;

    public bool useAsteroidMovement;

    void Start()
    {
        for (int i = 0; i < asteroidAmount; i++)
        {
            Transform temp = Instantiate(asteroids[Random.Range(0, asteroids.Length)], transform.position + Random.insideUnitSphere * fieldRadius, Random.rotation, this.transform);
            temp.localScale = temp.localScale * Random.Range(minScale, maxScale);
            if(useAsteroidMovement == false)
            {
                temp.GetComponent<AsteroidMovement>().enabled = false;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, fieldRadius);
    }

}
