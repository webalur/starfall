﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipAI : MonoBehaviour
{

    [SerializeField] Transform target;
    [SerializeField] float playerAttackDistance = 100;
    [SerializeField] float movementSpeed = 50f;
    [SerializeField] float rotationDamp = 0.5f;
    [SerializeField] float rotationSpeed = 15f;
    [SerializeField] float rayDistance = 40;
    [SerializeField] float rayCastOffset = 2.5f;

    Vector3 randomPosition;
    SpawnEnemies spawnEnemies;
    bool reachedRandomPosition;

    float startMovementSpeed;

    int HP = 500;
    int currentHP;

    Transform player;

    Vector3 pos;

    float distance;
    float anglePlayer;

    public int bulletSpeed = 70;

    public Transform gunRight;
    public Transform gunLeft;

    ObjectPooler objectPooler;
    CharactersPropertiesController charactersPropertiesController;

    Transform bullet;

    float attackTimer;
    bool changeGun;

    float timer;

    private void Start()
    {
        objectPooler = ObjectPooler.SharedInstance;

        spawnEnemies = FindObjectOfType<SpawnEnemies>();
        reachedRandomPosition = true;

        player = FindObjectOfType<FlyControler>().transform;

        charactersPropertiesController = FindObjectOfType<CharactersPropertiesController>();
        HP = charactersPropertiesController.turretHP;
        currentHP = HP;

        startMovementSpeed = movementSpeed;
    }

    public void GetDamage(int damage)
    {
        currentHP -= damage;
    }

    void Update()
    {
        if (currentHP <= 0)
        {
            GameObject ps = objectPooler.GetPooledObject(8);
            ps.transform.position = this.transform.position;
            ps.SetActive(true);
            Destroy(this.gameObject);
        }

        distance = Vector3.Distance(transform.position, player.position);
        anglePlayer = Vector3.Angle(player.transform.forward, transform.position);

        //attack player
        if (distance < playerAttackDistance)
            target = player;
        else
            target = null;

        //random position to go
        if (target == null && reachedRandomPosition)
        {
            randomPosition = spawnEnemies.FindRandomPosition();
            reachedRandomPosition = false;
        }

        if (Vector3.Distance(transform.position, randomPosition) < 10)
            reachedRandomPosition = true;

        //movement
        Pathfinding();
        Move();
    }

    void Turn()
    {
        if (target != null)
            pos = target.position - transform.position;
        else
            pos = randomPosition - transform.position;

        Quaternion rotation = Quaternion.LookRotation(pos);

        if (Mathf.Abs(anglePlayer) > playerAttackDistance)
        {
            if(distance < 60)
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 2 * Time.deltaTime);
            else
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationDamp * Time.deltaTime);
        }
        else
        { 
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 3 * Time.deltaTime);
        }

    }

    void Move()
    {
        if (target != null && distance < playerAttackDistance - 1)
        {
            if (startMovementSpeed > 0)
                startMovementSpeed -= Time.deltaTime * 12;
            if (startMovementSpeed < 0)
                startMovementSpeed = 0;
            
            transform.position += transform.forward * startMovementSpeed * Time.deltaTime;

            attackTimer += Time.deltaTime;
            if (attackTimer > 0.6f)
            {
                //gunLeft.LookAt(player);
                //gunRight.LookAt(player);
                Vector3 lTargetDir = player.position - gunLeft.position;
                //lTargetDir.y = 0.0f;
                gunLeft.rotation = Quaternion.RotateTowards(gunLeft.rotation, Quaternion.LookRotation(lTargetDir), Time.time * 4);

                lTargetDir = player.position - gunRight.position;
                //lTargetDir.y = 0.0f;
                gunRight.rotation = Quaternion.RotateTowards(gunRight.rotation, Quaternion.LookRotation(lTargetDir), Time.time * 4);


                attackTimer = 0;
                changeGun = !changeGun;

                bullet = objectPooler.GetPooledObject(7).transform;
                Rigidbody rb = bullet.GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
                if (changeGun)
                {
                    bullet.transform.position = gunRight.position;
                    bullet.transform.forward = gunRight.forward;
                }
                else
                {
                    bullet.transform.position = gunLeft.position;
                    bullet.transform.forward = gunLeft.forward;
                }
                bullet.gameObject.SetActive(true);
                rb.AddForce(bullet.transform.forward * bulletSpeed, ForceMode.Impulse);
            }
        }
        if (target == null && distance > playerAttackDistance)
        {
            if (startMovementSpeed < movementSpeed)
                startMovementSpeed += Time.deltaTime * 2;
            if (startMovementSpeed > movementSpeed)
                startMovementSpeed = movementSpeed;
            
            transform.position += transform.forward * startMovementSpeed * Time.deltaTime;
        }
    }

    void Pathfinding()
    {
        RaycastHit hit;
        
        Vector3 raycastOffset = Vector3.zero;

        Vector3 left = transform.position - transform.right * rayCastOffset;
        Vector3 right = transform.position + transform.right * rayCastOffset;
        Vector3 up = transform.position + transform.up * rayCastOffset;
        Vector3 down = transform.position - transform.up * rayCastOffset;

        Debug.DrawRay(left, transform.forward * rayDistance, Color.cyan);
        Debug.DrawRay(right, transform.forward * rayDistance, Color.cyan);
        Debug.DrawRay(up, transform.forward * rayDistance, Color.cyan);
        Debug.DrawRay(down, transform.forward * rayDistance, Color.cyan);

        int countRays = 0;

        if (Physics.Raycast(left, transform.forward, out hit, rayDistance))
        {
            raycastOffset += Vector3.right;
            countRays += 1;
        }
        if (Physics.Raycast(right, transform.forward, out hit, rayDistance))
        {
            raycastOffset -= Vector3.right;
            countRays += 1;
        }

        if (Physics.Raycast(up, transform.forward, out hit, rayDistance))
        {
            raycastOffset -= Vector3.up;
            countRays += 1;
        }
        if (Physics.Raycast(down, transform.forward, out hit, rayDistance))
        {
            raycastOffset += Vector3.up;
            countRays += 1;
        }

        if (countRays == 4)
        {
            randomPosition = spawnEnemies.FindRandomPosition();
            transform.Rotate(Vector3.up * rotationSpeed * 10 * Time.deltaTime);
        }
        else
        {
            if (raycastOffset != Vector3.zero)
                transform.Rotate(raycastOffset * rotationSpeed * Time.deltaTime);
            else
                Turn();
        }

    }

}
