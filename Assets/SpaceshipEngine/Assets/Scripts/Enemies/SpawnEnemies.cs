﻿using UnityEngine;
 using System;
 using System.Collections;
 using System.Collections.Generic;

public class SpawnEnemies : MonoBehaviour
{
    [Serializable]
    public class enemySpawn
    {
        public GameObject enemy;
        public int amount;
    }

    public enemySpawn[] spawns;

    public List<GameObject> allEnemies = new List<GameObject>();
    public List<GameObject> allSpaceShips = new List<GameObject>();

    SolidRectangleExample solidRectangleExample;

    float randomX;
    float randomY;
    float randomZ;

    float range;
    Vector3 pos;

    void Start()
    {
        solidRectangleExample = FindObjectOfType<SolidRectangleExample>();
        range = solidRectangleExample.range;
        pos = transform.position;

        //spawn enemies
        for (int i = 0; i < spawns.Length; i++)
        {
            for (int y = 0; y < spawns[i].amount; y++)
            {
                GameObject ship = Instantiate(spawns[i].enemy, FindRandomPosition(), Quaternion.identity, this.transform);
                allEnemies.Add(ship);
                allSpaceShips.Add(ship);
            }
        }

        for (int i = 0; i < FindObjectsOfType<TurretController>().Length; i++)
        {
            allEnemies.Add(FindObjectsOfType<TurretController>()[i].gameObject);
        }
    }

    public Vector3 FindRandomPosition()
    {
        range = solidRectangleExample.range;
        pos = transform.position;

        randomX = UnityEngine.Random.Range(pos.x - range, pos.x + range);
        randomY = UnityEngine.Random.Range(pos.y - range, pos.y + range);
        randomZ = UnityEngine.Random.Range(pos.z - range, pos.z + range);
      
        return new Vector3(randomX, randomY, randomZ);
    }

}
