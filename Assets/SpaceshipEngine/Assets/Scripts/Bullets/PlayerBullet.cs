﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{

    int damage;

    public bool rocket;

    GameObject ps;
    ObjectPooler objectPooler;
    GameObject obj;

    CharactersPropertiesController charactersPropertiesController;

    void Start()
    {
        objectPooler = ObjectPooler.SharedInstance;

        charactersPropertiesController = FindObjectOfType<CharactersPropertiesController>();
        if (rocket)
            damage = charactersPropertiesController.playerRocketDamage;
        else
            damage = charactersPropertiesController.playerBullerDamage;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(rocket)
            obj = objectPooler.GetPooledObject(5);
        else
            obj = objectPooler.GetPooledObject(9);
        obj.SetActive(true);
        obj.transform.position = this.transform.position;
        obj.transform.parent = objectPooler.transform;

        if(collision.gameObject.tag == "Turret")
        {
            collision.gameObject.GetComponent<TurretController>().GetDamage(damage);
        }
        if (collision.gameObject.tag == "EnemyShip")
        {
            collision.gameObject.GetComponent<SpaceshipAI>().GetDamage(damage);
        }

        this.gameObject.SetActive(false);
    }
}
