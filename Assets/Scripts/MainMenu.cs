using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // public GameObject MainMenuScene;
    // public GameObject SpaceScene;
    /*public GameObject PanelLevel1;
    public GameObject PanelLevel2;*/

    const int SCENE_MAIN_NUM = 0;
    const int SCENE_SPACE_NUM = 1;
    const int SCENE_PLANET_NUM = 2;

    void Start()
    {
        Time.timeScale = 1.0f;
    }

    public void PlaySpaceButton()
    {
        //MainMenuScene.SetActive(false);
        OnOpenScene(SCENE_SPACE_NUM);
    }

    public void PlayPlanetButton()
    {
        //MainMenuScene.SetActive(false);
        OnOpenScene(SCENE_PLANET_NUM);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    /* public void BackButton()
     {
         StartMenu.SetActive(true);
         SelectLevel.SetActive(false);
     }*/

    public void OnOpenScene(int num)
    {
        SceneManager.LoadScene(num);
    }

    /*public void Level2()
    {
    SceneManager.LoadScene(2);

    }
    public void PanelBackButton1()
    {
    PanelLevel1.SetActive(false);
    }
    public void PanelBackButton2()
    {
    PanelLevel2.SetActive(false);
    }
    public void StartPanelLevel1()
    {
    PanelLevel1.SetActive(true);
    }
    public void StartPanelLevel2()
    {
    PanelLevel2.SetActive(true);
    }*/
}