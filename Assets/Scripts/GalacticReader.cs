using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[SerializeField]
public class GalacticReader : MonoBehaviour
{
    public TextAsset jsonFile;
    public static string currentSun;
    SunSystem currentSunSystem;
    [SerializeField] GameObject[] PlanetPrefab;
    [SerializeField] GameObject[] SunPrefab;
    [SerializeField] GameObject[] MoonPrefab;
    GameObject spaceObject;
    public static List<string> suns = new List<string>();
    Galactic galacticInJson;
    public GameObject warpTunnel;
    public GameObject warpTunnelChild;
    public GameObject playerShip;
    public GameObject controller;

    public FlyControler flyControler;

    public float timeWarpRemaining = 10;
    public bool timerWarpIsRunning = false;

    static bool flagToChangeGalactic = false;
    float timeCounter;

    void Start()
    {
        galacticInJson = JsonUtility.FromJson<Galactic>(jsonFile.text);
        generateGalactic("Sun");

        flyControler = playerShip.GetComponentInChildren<FlyControler>();
    }

    void Update()
    {
        GameObject[] planets;
        planets = GameObject.FindGameObjectsWithTag("Planet");
        // timeCounter += Time.deltaTime * 2;

        foreach (GameObject planet in planets)
        {
            float angle = 0;
            int distance = 0;
            float speed = 0;
            foreach (Planet pl in currentSunSystem.planets)
            {
                if (pl.name == planet.name)
                {
                    angle = (float)pl.rotationSpeed;
                    speed = (float)pl.speed;
                    distance = (int)pl.distance;
                    break;
                }
            }

            // float timeCounter = Time.deltaTime * speed;
            // float x = Mathf.Cos(timeCounter) * distance;
            // float z = Mathf.Sin(timeCounter) * distance;
            // float y = 0;

            planet.transform.Rotate(0.0f, angle, 0.0f, Space.Self);
            // planet.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), speed * Time.deltaTime);
            // planet.transform.position = new Vector3(x, y, z);
        }

        GameObject[] stars = GameObject.FindGameObjectsWithTag("Sun");

        foreach (GameObject star in stars)
        {
            float angle = currentSunSystem.rotationSpeed;
            star.transform.Rotate(0.0f, angle, 0.0f, Space.Self);
        }



        if (flagToChangeGalactic)
        {
            flagToChangeGalactic = false;
            Debug.Log("CHANGE GALA ");
            cleanGalactic();
            playerShip.transform.position = new Vector3(45000, 0, 0);
            Vector3 newRotation = new Vector3(0, 0, 0);
            playerShip.transform.eulerAngles = newRotation;

            warpTunnel.transform.rotation = controller.transform.rotation;
            warpTunnel.transform.position = playerShip.transform.position;
            // warpTunnelChild.transform.rotation = controller.transform.rotation;
            // warpTunnelChild.transform.position = playerShip.transform.position;


            warpTunnel.SetActive(true);
            timerWarpIsRunning = true;
            timeWarpRemaining = 10;
        }

        if (timerWarpIsRunning)
        {
            if (timeWarpRemaining > 0)
            {
                timeWarpRemaining -= Time.deltaTime;
            }
            else
            {
                Debug.Log("Time has run out!");
                timeWarpRemaining = 0;
                timerWarpIsRunning = false;
                generateGalactic(currentSun);
                warpTunnel.SetActive(false);

                flyControler.inWarpByUIButton  = false;

            }
        }
    }

    void generateGalactic(string sun)
    {
        currentSun = sun;
        foreach (SunSystem sunSystem in galacticInJson.suns)
        {
            suns.Add(sunSystem.name);

            if (currentSun == sunSystem.name)
            {
                currentSunSystem = sunSystem;

                spaceObject = Instantiate(SunPrefab[sunSystem.number]) as GameObject;
                spaceObject.tag = "Sun";
                spaceObject.name = sunSystem.name;
                spaceObject.transform.position = new Vector3(0, 0, 0);
                spaceObject.transform.localScale = new Vector3(sunSystem.size, sunSystem.size, sunSystem.size);

                foreach (Planet planet in sunSystem.planets)
                {
                    spaceObject = Instantiate(PlanetPrefab[planet.number]) as GameObject;
                    spaceObject.tag = "Planet";
                    spaceObject.name = planet.name;
                    spaceObject.transform.position = new Vector3(planet.position.x, planet.position.y, planet.position.z);
                    spaceObject.transform.localScale = new Vector3(planet.size, planet.size, planet.size);
                }
            }
        }
    }

    void cleanGalactic()
    {
        // GameObject warpTunnel = GameObject.Find("WarpTunnel"); ;
        

        GameObject[] planets;
        planets = GameObject.FindGameObjectsWithTag("Planet");
        GameObject[] stars;
        stars = GameObject.FindGameObjectsWithTag("Sun");

        foreach (GameObject star in stars)
        {
            Destroy(star);
        }

        foreach (GameObject planet in planets)
        {
            Destroy(planet);
        }

        suns.Clear();
    }

    public static void changeGalactic(string sun)
    {
        

        currentSun = sun;
        flagToChangeGalactic = true;
    }
}
