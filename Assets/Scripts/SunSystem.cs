using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SunSystem
{
    //these variables are case sensitive and must match the strings "firstName" and "lastName" in the JSON.
    public string name;
    public int size;
    public string type;
    public int number;
    public Position position;
    public float rotationSpeed;
    public Planet[] planets;
}