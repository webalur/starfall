using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Planet
{
    public string name;
    public int size;
    public string type;
    public int number;
    public int distance;
    public Position position;
    public float rotationSpeed;
    public float speed;
    public Moon[] moons;
}