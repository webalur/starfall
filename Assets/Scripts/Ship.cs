using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField] public string name = "SF Fighter";
    [SerializeField] public int price = 100;
    [SerializeField] public string size = "S";
    [SerializeField] public float weight=20;
    [SerializeField] public float armor = 20;
    [SerializeField] public float shield = 20;



    [SerializeField] public float speed = 100;
    [SerializeField] public float furious = 10;
    [SerializeField] public float agility = 10;
    [SerializeField] public float HP = 200;
    //[SerializeField] public ShipClass shipClass;
    [SerializeField] public Sprite shipImage;
}
