using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Galactic 
{
    public SunSystem[] suns;
}