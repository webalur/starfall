using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Moon
{
    public string name;
    public int size;
    public int number;
    public int distance;
}