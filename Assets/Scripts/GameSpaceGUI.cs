using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameSpaceGUI : MonoBehaviour
{
    GUIContent content = new GUIContent();
    public Texture2D image;
    string text = "ButtonText";
    public GameObject PlayerShip;
    private CharactersPropertiesController charactersPropertiesController;
    [SerializeField] private GalacticReader galacticReader;
    private bool PAUSE_MODE = false;
    private bool MAP_MODE = false;
    public FlyControler flyControler;

    void Start()
    {
        flyControler = PlayerShip.GetComponentInChildren<FlyControler>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            
            PAUSE_MODE = !PAUSE_MODE;
            Cursor.lockState = PAUSE_MODE ? CursorLockMode.Confined : CursorLockMode.Locked;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            MAP_MODE = !MAP_MODE;
        }
    }

    private void PlayerControlReset()
    {
        //PlayerShip.GetComponentInChildren<FlyControler>().enabled = state;
        //sPlayerShip.GetComponentInChildren<MouseLook>().enabled = state;
    }

    void OnGUI()
    {
        GUI.skin.button.normal.background = (Texture2D)image;

        if (PAUSE_MODE)
        {
            flyControler.inWarpByKey = false;
            if (GUI.Button(new Rect(Screen.width - 110, 10, 100, 30), "Main menu"))
            {
                SceneManager.LoadScene(0);
            }
        }

        if (Cursor.lockState == CursorLockMode.Confined)
        {
            if (GUI.Button(new Rect(Screen.width - 110, 50, 100, 30), "continue game"))
            {
                PAUSE_MODE = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

        charactersPropertiesController = FindObjectOfType<CharactersPropertiesController>();
        int playerHP = charactersPropertiesController.playerHP;

        GUI.Label(new Rect(Screen.width / 2 - 50, 10, 100, 20), playerHP.ToString());
        GUI.Label(new Rect(10, 10, 100, 20), GalacticReader.currentSun.ToString());

        if (PAUSE_MODE)
        {
            int index = 0;
            foreach (string sun in GalacticReader.suns)
            {
                if (GUI.Button(new Rect(Screen.width - 110, Screen.height - GalacticReader.suns.Count * 40 + index * 40, 100, 30), sun))
                {
                    if(flyControler.inWarpByKey == false)
                    {
                        flyControler.inWarpByUIButton = true;
                        GalacticReader.changeGalactic(sun);
                        Debug.Log(sun);
                    } 
                    
                    //PlayerControlReset(false);
                   // Invoke("PlayerControlReset", galacticReader.timeWarpRemaining);
                }
                index++;
            }
               
        }

    }
}



