using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetControl : MonoBehaviour
{
    private MouseLook mouseLook;
    private FlyControler controler;

    void Start()
    {
        mouseLook = GetComponent<MouseLook>();
        controler = GetComponent<FlyControler>();
    }


    public void ResetPlayerControl(bool value)
    {

        mouseLook.enabled = value;
        controler.enabled = value;
    }

}
